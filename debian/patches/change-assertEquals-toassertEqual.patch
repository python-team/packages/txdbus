Description: change assertEquals() to assertEqual()
Author: Martin <debacle@debian.org>
Origin: vendor
Bug-Debian: https://bugs.debian.org/1074642
Last-Update: 2024-07-14
---
This patch header follows DEP-3: http://dep.debian.net/deps/dep3/
--- a/tests/client_tests.py
+++ b/tests/client_tests.py
@@ -206,7 +206,7 @@
             return ro.callRemote('testMethodSub', 'foo')
 
         def got_reply(reply):
-            self.assertEquals(reply, 'foobarSub')
+            self.assertEqual(reply, 'foobarSub')
 
         return self.proxy_chain(got_object, got_reply)
 
@@ -216,7 +216,7 @@
             return ro.callRemote('testMethod', 'foo')
 
         def got_reply(reply):
-            self.assertEquals(reply, 'foobar')
+            self.assertEqual(reply, 'foobar')
 
         return self.proxy_chain(got_object, got_reply)
 
@@ -266,7 +266,7 @@
             return ro.callRemote('GetManagedObjects')
 
         def got_reply(reply):
-            self.assertEquals(reply, {
+            self.assertEqual(reply, {
                 '/org/test/Foo/Bar': {
                     'org.freedesktop.DBus.Properties': {},
                     'org.txdbus.trial.Simple': {},
@@ -303,19 +303,19 @@
         f1 = yield ro1.callRemote('foo')
         f2 = yield ro2.callRemote('foo')
 
-        self.assertEquals(f1, 'foo')
-        self.assertEquals(f2, 'foo')
+        self.assertEqual(f1, 'foo')
+        self.assertEqual(f2, 'foo')
 
         self.server_conn.unexportObject('/org/test/Foo')
 
         f2 = yield ro2.callRemote('foo')
-        self.assertEquals(f2, 'foo')
+        self.assertEqual(f2, 'foo')
 
         try:
             f1 = yield ro1.callRemote('foo')
             self.fail('failed throw exception')
         except error.RemoteError as e:
-            self.assertEquals(
+            self.assertEqual(
                 e.message,
                 '/org/test/Foo is not an object provided by this process.')
         except Exception:
@@ -328,10 +328,10 @@
             dsig.callback(m)
 
         def check_results(m):
-            self.assertEquals(m.interface,
+            self.assertEqual(m.interface,
                               'org.freedesktop.DBus.ObjectManager')
-            self.assertEquals(m.member, 'InterfacesAdded')
-            self.assertEquals(m.body, [
+            self.assertEqual(m.member, 'InterfacesAdded')
+            self.assertEqual(m.body, [
                 '/org/test/Foo',
                 {
                     'org.txdbus.trial.SimpleSub': {'prop': 0},
@@ -369,10 +369,10 @@
             dsig.callback(m)
 
         def check_results(m):
-            self.assertEquals(m.interface,
+            self.assertEqual(m.interface,
                               'org.freedesktop.DBus.ObjectManager')
-            self.assertEquals(m.member, 'InterfacesRemoved')
-            self.assertEquals(m.body, [
+            self.assertEqual(m.member, 'InterfacesRemoved')
+            self.assertEqual(m.body, [
                 '/org/test/Foo', [
                     'org.txdbus.trial.SimpleSub',
                     'org.txdbus.trial.Simple',
@@ -425,7 +425,7 @@
             return ro.callRemote('testMethod', 'foo')
 
         def got_reply(reply):
-            self.assertEquals(reply, 'foobar')
+            self.assertEqual(reply, 'foobar')
 
         return self.proxy_chain(got_object, got_reply)
 
@@ -441,7 +441,7 @@
         d = self.server_conn.getNameOwner(self.tst_bus)
 
         def got_reply(reply):
-            self.assertEquals(reply, self.server_conn.busName)
+            self.assertEqual(reply, self.server_conn.busName)
 
         d.addCallback(got_reply)
 
@@ -453,7 +453,7 @@
             return ro.callRemote('testMethod', 'foo')
 
         def on_reply(reply):
-            self.assertEquals(reply, 'foobar')
+            self.assertEqual(reply, 'foobar')
 
         d = self.get_proxy(SimpleObjectTester.TestClass.tif)
         d.addCallback(on_proxy)
@@ -506,7 +506,7 @@
             )
 
         def on_err(e):
-            self.assertEquals(
+            self.assertEqual(
                 'org.freedesktop.DBus.Error.Failed: Already handled an Hello '
                 'message',
                 str(e.value),
@@ -667,7 +667,7 @@
         def gotxml(xml):
             # with open('/tmp/tout', 'w') as f:
             #    f.write(xml)
-            self.assertEquals(self.introspection_golden_xml, xml)
+            self.assertEqual(self.introspection_golden_xml, xml)
 
         d.addCallback(cb)
         d.addCallback(gotxml)
@@ -750,7 +750,7 @@
         d.addCallback(on_proxy)
 
         def check_result(result):
-            self.assertEquals(result, 'Signal arg: foo')
+            self.assertEqual(result, 'Signal arg: foo')
 
         dsig.addCallback(check_result)
 
@@ -848,7 +848,7 @@
 
         def check_signals_sent(result):
             # both callRemotes successful and returning None
-            self.assertEquals(len(result), 2)
+            self.assertEqual(len(result), 2)
             for success, returnValue in result:
                 self.assertTrue(success)
                 self.assertIsNone(returnValue)
@@ -859,8 +859,8 @@
 
         def check_signals_received(result):
             # d1 and d2 calledback with the correct signal data
-            self.assertEquals(result[0], (True, 'iface1'))
-            self.assertEquals(result[1], (True, 'iface2'))
+            self.assertEqual(result[0], (True, 'iface1'))
+            self.assertEqual(result[1], (True, 'iface2'))
             return result
 
         signalsReceived = defer.DeferredList([d1, d2])
@@ -892,7 +892,7 @@
         d.addCallback(on_proxy)
 
         def check_result(result):
-            self.assertEquals(result.body[0], 'Signal arg: MATCH')
+            self.assertEqual(result.body[0], 'Signal arg: MATCH')
 
         dsig.addCallback(check_result)
 
@@ -919,7 +919,7 @@
         d.addCallback(on_proxy)
 
         def check_result(result):
-            self.assertEquals(result.body[0], '/aa/bb/cc')
+            self.assertEqual(result.body[0], '/aa/bb/cc')
 
         dsig.addCallback(check_result)
 
@@ -952,7 +952,7 @@
         d.addCallback(on_proxy)
 
         def check_result(result):
-            self.assertEquals(result.body[0], 'Signal arg: MATCH')
+            self.assertEqual(result.body[0], 'Signal arg: MATCH')
 
         dsig.addCallback(check_result)
 
@@ -984,7 +984,7 @@
         d.addCallback(on_proxy)
 
         def check_result(result):
-            self.assertEquals(result.body[0], 'string payload')
+            self.assertEqual(result.body[0], 'string payload')
 
         dsig.addCallback(check_result)
 
@@ -1030,7 +1030,7 @@
 
     def test_err_no_method(self):
         def on_err(e):
-            self.assertEquals(
+            self.assertEqual(
                 'org.freedesktop.DBus.Error.UnknownMethod: Method "FooBarBaz" '
                 'with signature "" on interface "(null)" doesn\'t exist',
                 str(e.value),
@@ -1045,7 +1045,7 @@
 
     def test_err_no_object(self):
         def on_err(e):
-            self.assertEquals(
+            self.assertEqual(
                 'org.freedesktop.DBus.Error.UnknownObject: '
                 '/test/TestObjINVALID is not an object provided '
                 'by this process.',
@@ -1061,7 +1061,7 @@
 
     def test_err_call_no_arguments(self):
         def on_err(e):
-            self.assertEquals(
+            self.assertEqual(
                 str(e.value),
                 'org.freedesktop.DBus.Error.InvalidArgs: Call to errCall has '
                 'wrong args (, expected s)',
@@ -1076,7 +1076,7 @@
 
     def test_err_call_bad_arguments(self):
         def on_err(e):
-            self.assertEquals(
+            self.assertEqual(
                 str(e.value),
                 'org.freedesktop.DBus.Error.InvalidArgs: Call to errCall has '
                 'wrong args (i, expected s)',
@@ -1096,7 +1096,7 @@
 
     def test_raise_expected(self):
         def on_err(e):
-            self.assertEquals(
+            self.assertEqual(
                 str(e.value),
                 'org.txdbus.trial.TestException: ExpectedError',
             )
@@ -1112,7 +1112,7 @@
 
     def test_raise_python(self):
         def on_err(e):
-            self.assertEquals(
+            self.assertEqual(
                 str(e.value),
                 "org.txdbus.PythonException.KeyError: 'Uh oh!'",
             )
@@ -1127,7 +1127,7 @@
 
     def test_raise_invalid(self):
         def on_err(e):
-            self.assertEquals(
+            self.assertEqual(
                 str(e.value),
                 'org.txdbus.InvalidErrorName: !!(Invalid error name "oops")!! '
             )
@@ -1262,7 +1262,7 @@
         def got_reply(reply):
             expected = repr(u'foo') + ' # ' + \
                 repr([1, 2, [u'substring', 10], 4])
-            self.assertEquals(reply, expected)
+            self.assertEqual(reply, expected)
 
         return self.proxy_chain(got_object, got_reply)
 
@@ -1273,7 +1273,7 @@
             return ro.callRemote('testDictToTuples', d)
 
         def got_reply(reply):
-            self.assertEquals(
+            self.assertEqual(
                 reply,
                 [['baz', 'quux'], ['foo', 'bar'], ['william', 'wallace']],
             )
@@ -1287,11 +1287,11 @@
             return ro.callRemote('testDictToTuples2', d)
 
         def got_reply(reply):
-            self.assertEquals(
+            self.assertEqual(
                 reply[0],
                 [['baz', 'quux'], ['foo', 'bar'], ['william', 'wallace']],
             )
-            self.assertEquals(reply[1], 6)
+            self.assertEqual(reply[1], 6)
 
         return self.proxy_chain(got_object, got_reply)
 
@@ -1302,12 +1302,12 @@
             return ro.callRemote('testDictToTuples3', d)
 
         def got_reply(reply):
-            self.assertEquals(reply[0], 2)
-            self.assertEquals(
+            self.assertEqual(reply[0], 2)
+            self.assertEqual(
                 reply[1],
                 [['baz', 'quux'], ['foo', 'bar'], ['william', 'wallace']],
             )
-            self.assertEquals(reply[2], 6)
+            self.assertEqual(reply[2], 6)
 
         return self.proxy_chain(got_object, got_reply)
 
@@ -1319,12 +1319,12 @@
 
         def got_reply(reply_obj):
             reply = reply_obj[0]
-            self.assertEquals(reply[0], 2)
-            self.assertEquals(
+            self.assertEqual(reply[0], 2)
+            self.assertEqual(
                 reply[1],
                 [['baz', 'quux'], ['foo', 'bar'], ['william', 'wallace']],
             )
-            self.assertEquals(reply[2], 6)
+            self.assertEqual(reply[2], 6)
 
         return self.proxy_chain(got_object, got_reply)
 
@@ -1395,7 +1395,7 @@
             return ro.callRemote('notImplemented')
 
         def on_err(err):
-            self.assertEquals(
+            self.assertEqual(
                 err.getErrorMessage(),
                 'org.txdbus.PythonException.NotImplementedError',
             )
@@ -1581,7 +1581,7 @@
             return ro.callRemote('renamedMethod')
 
         def got_reply(reply):
-            self.assertEquals(reply, 'sneaky')
+            self.assertEqual(reply, 'sneaky')
 
         return self.proxy_chain(got_object, got_reply)
 
@@ -1597,7 +1597,7 @@
             return ro2.callRemote('baz')
 
         def got_reply(reply):
-            self.assertEquals(reply, 'iface1')
+            self.assertEqual(reply, 'iface1')
 
         return self.proxy_chain(got_object, got_object2, got_reply)
 
@@ -1610,7 +1610,7 @@
             )
 
         def on_err(err):
-            self.assertEquals(
+            self.assertEqual(
                 err.getErrorMessage(),
                 'Introspection failed to find interfaces: org.txdbus.'
                 'INVALID_INTERFACE',
@@ -1633,7 +1633,7 @@
             )
 
         def on_err(err):
-            self.assertEquals(
+            self.assertEqual(
                 err.getErrorMessage(),
                 'org.txdbus.PythonException.NotImplementedError',
             )
@@ -1649,7 +1649,7 @@
                 self.t.emitSignal('InvalidSignalName')
                 self.assertTrue(False, 'Should have raised an exception')
             except AttributeError as e:
-                self.assertEquals(
+                self.assertEqual(
                     str(e),
                     'Signal "InvalidSignalName" not found in any supported '
                     'interface.',
@@ -1662,7 +1662,7 @@
             return ro.callRemote('baz')
 
         def got_reply(reply):
-            self.assertEquals(reply, 'iface1')
+            self.assertEqual(reply, 'iface1')
 
         return self.proxy_chain(got_object, got_reply)
 
@@ -1671,7 +1671,7 @@
             return ro.callRemote('baz', interface='org.txdbus.trial.IFace2')
 
         def got_reply(reply):
-            self.assertEquals(reply, 'iface2')
+            self.assertEqual(reply, 'iface2')
 
         return self.proxy_chain(got_object, got_reply)
 
@@ -1716,7 +1716,7 @@
             return ro.callRemote('testMethod', 'foo')
 
         def got_reply(reply):
-            self.assertEquals(reply, 'foobar')
+            self.assertEqual(reply, 'foobar')
 
         return self.proxy_chain(got_object, got_reply)
 
@@ -1726,7 +1726,7 @@
             return ro.callRemote('blah')
 
         def got_reply(reply):
-            self.assertEquals(reply, ['foo', 'bar'])
+            self.assertEqual(reply, ['foo', 'bar'])
 
         return self.proxy_chain(got_object, got_reply)
 
@@ -1736,7 +1736,7 @@
             return ro.callRemote('Get', 'org.txdbus.trial.IFace2', 'prop1')
 
         def got_reply(reply):
-            self.assertEquals(reply, 'foobar')
+            self.assertEqual(reply, 'foobar')
 
         return self.proxy_chain(got_object, got_reply)
 
@@ -1746,7 +1746,7 @@
             return ro.callRemote('Get', 'org.txdbus.trial.IFace1', 'common')
 
         def got_reply(reply):
-            self.assertEquals(reply, 'common1')
+            self.assertEqual(reply, 'common1')
 
         return self.proxy_chain(got_object, got_reply)
 
@@ -1756,7 +1756,7 @@
             return ro.callRemote('Get', 'org.txdbus.trial.IFace2', 'common')
 
         def got_reply(reply):
-            self.assertEquals(reply, 'common2')
+            self.assertEqual(reply, 'common2')
 
         return self.proxy_chain(got_object, got_reply)
 
@@ -1766,7 +1766,7 @@
             return ro.callRemote('Get', '', 'prop1')
 
         def got_reply(reply):
-            self.assertEquals(reply, 'foobar')
+            self.assertEqual(reply, 'foobar')
 
         return self.proxy_chain(got_object, got_reply)
 
@@ -1776,7 +1776,7 @@
             return ro.callRemote('Get', '', 'prop2')
 
         def got_reply(reply):
-            self.assertEquals(reply, 5)
+            self.assertEqual(reply, 5)
 
         return self.proxy_chain(got_object, got_reply)
 
@@ -1786,7 +1786,7 @@
             return ro.callRemote('Get', '', 'INVALID_PROPERTY')
 
         def got_err(err):
-            self.assertEquals(
+            self.assertEqual(
                 err.getErrorMessage(),
                 'org.txdbus.PythonException.Exception: Invalid Property')
 
@@ -1801,7 +1801,7 @@
             return ro.callRemote('Set', '', 'INVALID_PROPERTY', 'Whoopsie')
 
         def got_err(err):
-            self.assertEquals(
+            self.assertEqual(
                 err.getErrorMessage(),
                 'org.txdbus.PythonException.Exception: Invalid Property')
 
@@ -1816,7 +1816,7 @@
             return ro.callRemote('Set', '', 'prop1', 'Whoopsie')
 
         def got_err(err):
-            self.assertEquals(
+            self.assertEqual(
                 err.getErrorMessage(),
                 'org.txdbus.PythonException.Exception: Property is not '
                 'Writeable',
@@ -1833,7 +1833,7 @@
             return ro.callRemote('Get', '', 'wronly')
 
         def got_err(err):
-            self.assertEquals(
+            self.assertEqual(
                 err.getErrorMessage(),
                 'org.txdbus.PythonException.Exception: Property is not '
                 'readable',
@@ -1847,11 +1847,11 @@
     def test_set_string_property(self):
 
         def got_object(ro):
-            self.assertEquals(self.t.propw, 'orig')
+            self.assertEqual(self.t.propw, 'orig')
             return ro.callRemote('Set', '', 'pwrite', 'changed')
 
         def got_reply(reply):
-            self.assertEquals(self.t.propw, 'changed')
+            self.assertEqual(self.t.propw, 'changed')
 
         return self.proxy_chain(got_object, got_reply)
 
@@ -1869,7 +1869,7 @@
 
             # The remaining properties have no possible ambiguity.
             del reply['common']
-            self.assertEquals(reply, {
+            self.assertEqual(reply, {
                 'prop1': 'foobar',
                 'prop2': 5,
                 'prop_if1': 'pif1',
@@ -1887,9 +1887,9 @@
         def check_results(arg):
             interface_name, changed, invalidated = arg
 
-            self.assertEquals(interface_name, 'org.txdbus.trial.IFace2')
-            self.assertEquals(changed, {'pwrite': 'should emit'})
-            self.assertEquals(invalidated, [])
+            self.assertEqual(interface_name, 'org.txdbus.trial.IFace2')
+            self.assertEqual(changed, {'pwrite': 'should emit'})
+            self.assertEqual(invalidated, [])
 
         def on_proxy(ro):
             dnot = ro.notifyOnSignal('PropertiesChanged', on_signal)
@@ -1933,14 +1933,14 @@
         d = self.client_conn.requestBusName('org.test.foobar')
 
         def cb(i):
-            self.assertEquals(i, client.NAME_ACQUIRED)
+            self.assertEqual(i, client.NAME_ACQUIRED)
 
         d.addCallback(cb)
         return d
 
     def test_bad_bus_name(self):
         def on_err(e):
-            self.assertEquals(
+            self.assertEqual(
                 'org.freedesktop.DBus.Error.InvalidArgs: Cannot acquire a '
                 'service starting with \':\' such as ":1.234"',
                 str(e.value)
@@ -1956,7 +1956,7 @@
         d = self.client_conn.requestBusName('org.test.foobar')
 
         def cb(i):
-            self.assertEquals(i, client.NAME_ALREADY_OWNER)
+            self.assertEqual(i, client.NAME_ALREADY_OWNER)
 
         d.addCallback(
             lambda _: self.client_conn.requestBusName('org.test.foobar'))
@@ -1989,7 +1989,7 @@
             lambda _: self.client_conn2.requestBusName('org.test.foobar'))
 
         def on_err(e):
-            self.assertEquals(
+            self.assertEqual(
                 'Failed to acquire bus name "org.test.foobar": Name in use',
                 str(e.value),
             )
@@ -2011,7 +2011,7 @@
         flags = {'sig': False}
 
         def on_name_lost(sig):
-            self.assertEquals(sig.body[0], 'org.test.foobar')
+            self.assertEqual(sig.body[0], 'org.test.foobar')
             flags['sig'] = True
 
         d.addCallback(
@@ -2033,7 +2033,7 @@
 
         def cb(i):
             self.assertTrue(flags['sig'])
-            self.assertEquals(i, client.NAME_ACQUIRED)
+            self.assertEqual(i, client.NAME_ACQUIRED)
 
         d.addCallback(cb)
 
@@ -2051,7 +2051,7 @@
         flags = {'sig': False}
 
         def on_name_acq(sig):
-            self.assertEquals(sig.body[0], 'org.test.foobar')
+            self.assertEqual(sig.body[0], 'org.test.foobar')
             flags['sig'] = True
 
         d.addCallback(
@@ -2071,7 +2071,7 @@
             errbackUnlessAcquired=False,
         ))
 
-        d.addCallback(lambda r: self.assertEquals(
+        d.addCallback(lambda r: self.assertEqual(
             r,
             client.NAME_IN_QUEUE,
             'Queue error'
@@ -2081,7 +2081,7 @@
             'org.test.foobar'
         ))
 
-        d.addCallback(lambda r: self.assertEquals(
+        d.addCallback(lambda r: self.assertEqual(
             r,
             client.NAME_RELEASED,
             'Failed to release name',
@@ -2108,7 +2108,7 @@
         flags = {'sig': False}
 
         def on_name_acq(sig):
-            self.assertEquals(sig.body[0], 'org.test.foobar')
+            self.assertEqual(sig.body[0], 'org.test.foobar')
             flags['sig'] = True
 
         d.addCallback(lambda _: self.client_conn2.addMatch(
@@ -2127,7 +2127,7 @@
             doNotQueue=False,
         ))
 
-        d.addErrback(lambda e: self.assertEquals(
+        d.addErrback(lambda e: self.assertEqual(
             e.getErrorMessage(),
             'Failed to acquire bus name "org.test.foobar": Queued for name '
             'acquisition'
@@ -2137,7 +2137,7 @@
             'org.test.foobar'
         ))
 
-        d.addCallback(lambda r: self.assertEquals(
+        d.addCallback(lambda r: self.assertEqual(
             r,
             client.NAME_RELEASED,
             'Failed to release name',
@@ -2173,7 +2173,7 @@
             errbackUnlessAcquired=False,
         ))
 
-        d.addCallback(lambda r: self.assertEquals(
+        d.addCallback(lambda r: self.assertEqual(
             r,
             client.NAME_IN_QUEUE,
             'Queue error',
@@ -2183,7 +2183,7 @@
             'org.test.foobar',
         ))
 
-        d.addCallback(lambda r: self.assertEquals(
+        d.addCallback(lambda r: self.assertEqual(
             r,
             [self.client_conn.busName, self.client_conn2.busName],
             'Bus Name Queue differes from expected value'
@@ -2202,7 +2202,7 @@
         d = self.client_conn.listQueuedBusNameOwners('org.test.foobar')
 
         def on_err(e):
-            self.assertEquals(
+            self.assertEqual(
                 'org.freedesktop.DBus.Error.NameHasNoOwner: Could not get '
                 'owners of name \'org.test.foobar\': no such name',
                 str(e.value),
@@ -2229,7 +2229,7 @@
             'org.test.foobar',
         ))
 
-        d.addCallback(lambda r: self.assertEquals(
+        d.addCallback(lambda r: self.assertEqual(
             r,
             os.getuid(),
             'Failed to get connection user id',
@@ -2260,7 +2260,7 @@
         ))
 
         def on_err(e):
-            self.assertEquals(
+            self.assertEqual(
                 'org.freedesktop.DBus.Error.NameHasNoOwner: Could not get UID '
                 'of name \'org.MISSING_BUS_NAME\': no such name',
                 str(e.value),
--- a/tests/test_authentication.py
+++ b/tests/test_authentication.py
@@ -45,10 +45,10 @@
         self.ca.handleAuthMessage(msg)
 
     def ae(self, x, y):
-        self.assertEquals(x, y)
+        self.assertEqual(x, y)
 
     def are(self, x):
-        self.assertEquals(self.reply, x)
+        self.assertEqual(self.reply, x)
 
     def test_bad_auth_message(self):
         self.assertRaises(DBusAuthenticationFailed, self.send, b'BAD_LINE')
@@ -176,10 +176,10 @@
         self.ba = authentication.BusCookieAuthenticator()
 
     def ae(self, x, y):
-        self.assertEquals(x, y)
+        self.assertEqual(x, y)
 
     def ar(self, x):
-        self.assertEquals(x, ('REJECTED', None))
+        self.assertEqual(x, ('REJECTED', None))
 
     def s(self, x):
         return self.ba.step(x)
@@ -277,11 +277,11 @@
         self.ca.handleAuthMessage(msg)
 
     def test_dbus_cookie_authentication(self):
-        self.assertEquals(self.ba.getMechanismName(), 'DBUS_COOKIE_SHA1')
+        self.assertEqual(self.ba.getMechanismName(), 'DBUS_COOKIE_SHA1')
 
         while not self.ca.authMech == b'DBUS_COOKIE_SHA1':
             self.ca.authTryNextMethod()
-        self.assertEquals(
+        self.assertEqual(
             self.reply,
             b'AUTH DBUS_COOKIE_SHA1 ' +
             tohex(b'testuser'),
@@ -292,10 +292,10 @@
         try:
             self.ca.cookie_dir = k
             s1 = self.ba._step_one('0', k)
-            self.assertEquals(s1[0], 'CONTINUE')
+            self.assertEqual(s1[0], 'CONTINUE')
             self.send(b'DATA ' + tohex(s1[1]))
             self.assertTrue(self.reply.startswith(b'DATA'))
-            self.assertEquals(
+            self.assertEqual(
                 self.ba._step_two(unhex(self.reply.split()[1])),
                 ('OK', None),
             )
@@ -324,7 +324,7 @@
         self.ba._create_cookie(g(20.0))
         self.ba._create_cookie(g(21.2))
         c = self.ba._get_cookies()
-        self.assertEquals({b'3', b'4'}, {x[0] for x in c})
+        self.assertEqual({b'3', b'4'}, {x[0] for x in c})
 
     def test_del_cookie_with_remaining(self):
         self.ba._create_cookie()
@@ -333,7 +333,7 @@
         self.ba.cookieId = 2
         self.ba._delete_cookie()
         c = self.ba._get_cookies()
-        self.assertEquals({b'1', b'3'}, {x[0] for x in c})
+        self.assertEqual({b'1', b'3'}, {x[0] for x in c})
 
     def test_del_cookie_last(self):
         self.ba._create_cookie()
@@ -348,24 +348,24 @@
     def test_external_auth_logic(self):
         bea = authentication.BusExternalAuthenticator()
 
-        self.assertEquals(bea.getMechanismName(), 'EXTERNAL')
+        self.assertEqual(bea.getMechanismName(), 'EXTERNAL')
 
         class T(object):
             _unix_creds = None
 
         bea.init(T())
 
-        self.assertEquals(
+        self.assertEqual(
             bea.step(''),
             ('REJECT', 'Unix credentials not available'),
         )
 
         bea.creds = ('foo', 0)
 
-        self.assertEquals(bea.step(''), ('CONTINUE', ''))
-        self.assertEquals(bea.step(''), ('OK', None))
+        self.assertEqual(bea.step(''), ('CONTINUE', ''))
+        self.assertEqual(bea.step(''), ('OK', None))
 
-        self.assertEquals(bea.getUserName(), 'root')
+        self.assertEqual(bea.getUserName(), 'root')
 
         bea.cancel()
 
@@ -375,13 +375,13 @@
     def test_anonymous_auth_logic(self):
         baa = authentication.BusAnonymousAuthenticator()
 
-        self.assertEquals(baa.getMechanismName(), 'ANONYMOUS')
+        self.assertEqual(baa.getMechanismName(), 'ANONYMOUS')
 
         baa.init(None)
 
-        self.assertEquals(baa.step(''), ('OK', None))
+        self.assertEqual(baa.step(''), ('OK', None))
 
-        self.assertEquals(baa.getUserName(), 'anonymous')
+        self.assertEqual(baa.getUserName(), 'anonymous')
 
         baa.cancel()
 
@@ -501,7 +501,7 @@
         self.send(b'FISHY')
 
         def recv(msg):
-            self.assertEquals(msg, b'ERROR "Unknown command"')
+            self.assertEqual(msg, b'ERROR "Unknown command"')
             d.callback(None)
         self.gotMessage = recv
         return d
--- a/tests/test_endpoints.py
+++ b/tests/test_endpoints.py
@@ -33,7 +33,7 @@
             endpoints.getDBusEnvEndpoints(reactor)
             self.assertTrue(False)
         except Exception as e:
-            self.assertEquals(
+            self.assertEqual(
                 str(e),
                 'DBus Session environment variable not set',
             )
@@ -44,38 +44,38 @@
     @skip("Skipping")
     def test_unix_address(self):
         e = self.gde('unix:path=/var/run/dbus/system_bus_socket')[0]
-        self.assertEquals(e._path, '/var/run/dbus/system_bus_socket')
+        self.assertEqual(e._path, '/var/run/dbus/system_bus_socket')
         e = self.gde('unix:tmpdir=/tmp')[0]
         self.assertTrue(e._path.startswith('/tmp/dbus-'))
         e = self.gde(
             'unix:abstract=/tmp/dbus-jgAbdgyUH7,'
             'guid=6abbe624c672777bd87ab46e00027706'
         )[0]
-        self.assertEquals(e._path, '\0/tmp/dbus-jgAbdgyUH7')
+        self.assertEqual(e._path, '\0/tmp/dbus-jgAbdgyUH7')
         e = self.gde('unix:abstract=/tmp/dbus-jgAbdgyUH7', False)[0]
-        self.assertEquals(e._address, '\0/tmp/dbus-jgAbdgyUH7')
+        self.assertEqual(e._address, '\0/tmp/dbus-jgAbdgyUH7')
         self.assertTrue(isinstance(e, UNIXServerEndpoint))
 
     @skip("Skipping")
     def test_tcp_address(self):
         e = self.gde('tcp:host=127.0.0.1,port=1234')[0]
-        self.assertEquals(e._host, '127.0.0.1')
-        self.assertEquals(e._port, 1234)
+        self.assertEqual(e._host, '127.0.0.1')
+        self.assertEqual(e._port, 1234)
 
         e = self.gde('tcp:host=127.0.0.1,port=1234', False)[0]
-        self.assertEquals(e._interface, '127.0.0.1')
-        self.assertEquals(e._port, 1234)
+        self.assertEqual(e._interface, '127.0.0.1')
+        self.assertEqual(e._port, 1234)
 
     def test_nonce_tcp_address(self):
         e = self.gde('nonce-tcp:host=127.0.0.1,port=1234,noncefile=/foo')[0]
-        self.assertEquals(e._host, '127.0.0.1')
-        self.assertEquals(e._port, 1234)
+        self.assertEqual(e._host, '127.0.0.1')
+        self.assertEqual(e._port, 1234)
         self.assertTrue('noncefile' in e.dbus_args)
-        self.assertEquals(e.dbus_args['noncefile'], '/foo')
+        self.assertEqual(e.dbus_args['noncefile'], '/foo')
 
     def test_launchd_address(self):
         l = self.gde('launchd:env=foo')
-        self.assertEquals(l, [])
+        self.assertEqual(l, [])
 
     def test_session(self):
         self.env(
@@ -83,14 +83,14 @@
             'guid=6abbe624c672777bd87ab46e00027706'
         )
         e = self.gde('session')[0]
-        self.assertEquals(e._path, '\0/tmp/dbus-jgAbdgyUH7')
+        self.assertEqual(e._path, '\0/tmp/dbus-jgAbdgyUH7')
 
         self.env(None)
         self.assertRaises(Exception, self.gde, 'session')
 
     def test_system(self):
         e = self.gde('system')[0]
-        self.assertEquals(e._path, '/var/run/dbus/system_bus_socket')
+        self.assertEqual(e._path, '/var/run/dbus/system_bus_socket')
 
     def test_multiple_addresses(self):
         self.env(
@@ -101,7 +101,7 @@
         l = self.gde('session')
         self.assertTrue(len(l) == 2)
         e = l[0]
-        self.assertEquals(e._path, '\0/tmp/dbus-jgAbdgyUH7')
+        self.assertEqual(e._path, '\0/tmp/dbus-jgAbdgyUH7')
         e = l[1]
-        self.assertEquals(e._host, '127.0.0.1')
-        self.assertEquals(e._port, 1234)
+        self.assertEqual(e._host, '127.0.0.1')
+        self.assertEqual(e._port, 1234)
--- a/tests/test_marshal.py
+++ b/tests/test_marshal.py
@@ -28,7 +28,7 @@
 class SigFromPyTests(unittest.TestCase):
 
     def t(self, p, s):
-        self.assertEquals(m.sigFromPy(p), s)
+        self.assertEqual(m.sigFromPy(p), s)
 
     def test_int(self):
         self.t(1, 'i')
@@ -84,25 +84,25 @@
 class AlignmentTests(unittest.TestCase):
 
     def test_no_padding(self):
-        self.assertEquals(m.pad['y'](1), b'')
+        self.assertEqual(m.pad['y'](1), b'')
 
     def test_2align(self):
-        self.assertEquals(m.pad['n'](1), b'\0')
+        self.assertEqual(m.pad['n'](1), b'\0')
 
     def test_8align(self):
-        self.assertEquals(m.pad['t'](1), b'\0' * 7)
+        self.assertEqual(m.pad['t'](1), b'\0' * 7)
 
     def test_0align(self):
-        self.assertEquals(m.pad['t'](8), b'')
+        self.assertEqual(m.pad['t'](8), b'')
 
     def test_mid_align(self):
-        self.assertEquals(m.pad['t'](4), b'\0' * 4)
+        self.assertEqual(m.pad['t'](4), b'\0' * 4)
 
 
 class SignatureIteratorTests(unittest.TestCase):
 
     def ae(self, sig, expected):
-        self.assertEquals(list(m.genCompleteTypes(sig)), expected)
+        self.assertEqual(list(m.genCompleteTypes(sig)), expected)
 
     def test_one(self):
         self.ae('i', ['i'])
@@ -136,7 +136,7 @@
             var_list = [var_list]
         nbytes, chunks = m.marshal(sig, var_list, 0, little_endian)
         bin_str = b''.join(chunks)
-        self.assertEquals(
+        self.assertEqual(
             nbytes,
             len(expected_encoding),
             "Byte length mismatch. Expected %d. Got %d" % (
@@ -144,7 +144,7 @@
                 nbytes,
             ),
         )
-        self.assertEquals(
+        self.assertEqual(
             bin_str,
             expected_encoding,
             "Binary encoding differs from expected value",
@@ -355,7 +355,7 @@
 
     def check(self, sig, expected_value, encoding):
         nbytes, value = m.unmarshal(sig, encoding, 0)
-        self.assertEquals(
+        self.assertEqual(
             nbytes,
             len(encoding),
             (
--- a/tests/test_message.py
+++ b/tests/test_message.py
@@ -25,4 +25,4 @@
             message.parseMessage(E('foo.bar', 5).rawMessage, oobFDs=[])
             self.assertTrue(False)
         except Exception as e:
-            self.assertEquals(str(e), 'Unknown Message Type: 99')
+            self.assertEqual(str(e), 'Unknown Message Type: 99')
--- a/tests/test_validators.py
+++ b/tests/test_validators.py
@@ -56,7 +56,7 @@
         self.t('/foo~bar')
 
     def test_5(self):
-        self.assertEquals(marshal.validateObjectPath('/foo/bar'), None)
+        self.assertEqual(marshal.validateObjectPath('/foo/bar'), None)
 
 
 class ErrorNameValidationTester (InterfaceNameValidationTester):
